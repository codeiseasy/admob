package com.codeiseasydev.admobhelper


import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.admobhelper.Constants.privacyPolicy
import com.codeiseasydev.admobhelper.Constants.publisherId
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.UserConsentSdk
import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.ui.AdMobRequest
import com.google.android.ads.consent.ui.UserConsentDialogStyle


open class UserConsentActivity : AppCompatActivity() {
    private var userConsent: UserConsentSdk? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var style = UserConsentDialogStyle(this)
        style.setTopBarDisplayIcon(false)
        style.setButtonPersonalizedIAgreeText("Yes, I Accept")
        style.setButtonPersonalizedDisagreeText("No, Thanks")
        style.setButtonPersonalizedAgreeText("Accept")
        style.setTopBarTextColor(resources.getColor(R.color.user_consent_top_bar_text_color))
        style.setPrivacyTextColor(resources.getColor(R.color.user_consent_privacy_text_color))
        style.setBodyTextColor(resources.getColor(R.color.user_consent_body_text_color))

        style.setTopBarBackgroundColor(resources.getColor(R.color.user_consent_top_bar_background_color))
        style.setButtonBackgroundColor(resources.getColor(R.color.user_consent_button_background_color))
        style.setButtonTextColor(resources.getColor(R.color.user_consent_button_text_color))

        style.setTopBarTextSize(18f)
        style.setBodyTextSize(16f)
        style.setPrivacyTextSize(16f)
        style.setTopBarTextFont("fonts/HacenTunisiaLt.ttf")
        style.setBodyTextFont("fonts/HacenTunisiaLt.ttf")
        style.setPrivacyTextFont("fonts/HacenTunisiaLt.ttf")
        style.setAccentColor(resources.getColor(R.color.user_consent_accent_color))
        style.setButtonCornerRadius(4)
        style.setAllCornerRadius(0)

        userConsent = UserConsentSdk.Builder()
            .setContext(this)
            .setCaliforniaConsumerPrivacyAct(true)
            .setPrivacyUrl(privacyPolicy) // Add your privacy policy url
            .setPublisherId(publisherId) // Add your admob publisher id
            .setTestDeviceId(AdMobRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
            .setLog(UserConsentActivity::class.java.name) // Add custom tag default: ID_LOG
            .setDebugGeography(true) // Geography appears as in EEA for test devices.
            .setShowingWindowWithAnimation(true)
            .setShowingWindowMode(WindowMode.SHEET_BOTTOM)
            .setUserConsentStyle(style)
            .build()
    }

    fun checkUserConsent(callback: UserConsentEventListener?) {
        userConsent?.checkUserConsent(object : UserConsentEventListener {
            override fun onResult(consentStatus: ConsentStatus, isRequestLocationInEeaOrUnknown: Boolean) {
                callback?.onResult(consentStatus, isRequestLocationInEeaOrUnknown)
            }

            override fun onFailed(reason: String) {
                callback?.onFailed(reason)
            }
        })

    }

    fun revokeUserConsent() {
        userConsent?.revokeUserConsent()
    }

}