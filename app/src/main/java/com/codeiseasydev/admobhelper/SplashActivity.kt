package com.codeiseasydev.admobhelper

import android.content.Intent
import android.os.Bundle
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.prefs.UserConsentPreferences


class SplashActivity : UserConsentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (UserConsentPreferences.getInstance(this@SplashActivity).isUserConfirmAgreement()){
             goToMain()
        } else {
            checkUserConsent(object : UserConsentEventListener {
                override fun onResult(consentStatus: ConsentStatus, isRequestLocationInEeaOrUnknown: Boolean) {
                    if (isRequestLocationInEeaOrUnknown){
                        UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                        goToMain()
                    }
                }

                override fun onFailed(reason: String) {
                    UserConsentPreferences.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }
            })
        }
    }

    fun goToMain(){
        startActivity(Intent(this@SplashActivity, AdMobActivity::class.java))
        finish()
    }
}