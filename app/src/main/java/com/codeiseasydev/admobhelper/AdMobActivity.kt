package com.codeiseasydev.admobhelper

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.admobhelper.Constants.privacyPolicy
import com.codeiseasydev.admobhelper.Constants.publisherId
import com.google.android.ads.admob.AdMobHelper
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.ads.admob.listener.AdMobListener
import com.google.android.ads.admob.listener.AdMobRewardedListener
import com.google.android.ads.admob.listener.AdMobNativeListener
import com.google.android.ads.admob.templates.AdMobNativeDisplaySize
import com.google.android.ads.admob.templates.AdMobNativeTemplateStyle
import com.google.android.ads.admob.enums.AdMobSize
import com.google.android.ads.consent.UserConsentSdk
import com.google.android.ads.consent.ui.UserConsentSettingsActivity
import kotlinx.android.synthetic.main.activity_main.*

class AdMobActivity : AppCompatActivity() {
    private var adMobHelper: AdMobHelper? = null
    private val buttonLoadInterstitialAd by lazy { findViewById<Button>(R.id.btn_load_interstitial) }
    private val buttonLoadRewardedVideoAd by lazy { findViewById<Button>(R.id.btn_load_rewarded_video) }
    private val buttonSettingConsentAd by lazy { findViewById<Button>(R.id.btn_setting_consent) }
    private val buttonRevokeConsentAd by lazy { findViewById<Button>(R.id.btn_revoke_consent) }

    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            resources.displayMetrics
        )
    }

    private fun initViews(){
        buttonLoadInterstitialAd.setOnClickListener {
            adMobHelper?.loadInterstitialAfterEndCount(it, 1, object : AdMobListener(){
                override fun onAdClosed() {
                    super.onAdClosed()
                    AdMobLogDebug.d("ADMOB", "InterstitialAd onAdClosed()")
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    super.onAdFailedToLoad(errorCode)
                    AdMobLogDebug.d("ADMOB", "InterstitialAd onAdFailedToLoad($errorCode)")
                }
            })
            return@setOnClickListener
        }
        buttonLoadRewardedVideoAd.setOnClickListener {
            adMobHelper?.loadRewardedVideoAfterEndCount(it, 1, object : AdMobRewardedListener() {
                override fun onRewardedVideoAdClosed() {
                    super.onRewardedVideoAdClosed()
                    AdMobLogDebug.d("ADMOB", "RewardedVideoAd onAdClosed()")
                }
            })
            return@setOnClickListener
        }
        buttonSettingConsentAd.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }
        buttonRevokeConsentAd.setOnClickListener {
            UserConsentSdk.getInstance(this).revokeUserConsentDialog()
        }
    }

    private fun initAds(){
        var colorDrawable = ColorDrawable(Color.parseColor("#C8ffffff"))
        val primaryTypeFace = Typeface.createFromAsset(assets, "fonts/Almarai-Regular.ttf")
        val secondaryTypeFace = Typeface.createFromAsset(assets, "fonts/Barlow-Regular.ttf")

        val nativeTemplateStyle = AdMobNativeTemplateStyle.Builder()
            .withMainBackgroundColor(colorDrawable)
            .withPrimaryTextTypeface(primaryTypeFace)
            .withCallToActionTextTypeface(secondaryTypeFace)
            .withSecondaryTextTypeface(secondaryTypeFace)
            .withTertiaryTextTypeface(secondaryTypeFace)
            .withBadgeTextTypeface(secondaryTypeFace)
            .build()

        adMobHelper = AdMobHelper.Builder()
            .setContext(this)
            .setDebugTestAds(true)
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setLoadAnimationBeforeLoadAd(true)
            .setLoadAnimationBeforeLoadAdAccentColor(R.color.colorPrimaryDark)
            .setAdViewSize(AdMobSize.LARGE_BANNER)
            .setEnableAds(true)
            .setEnableBannerAd(true)
            .setEnableInterstitialAd(true)
            .setEnableRewardedVideoAd(true)
            .setNativeAdvancedStyles(nativeTemplateStyle)
            .build()

        adMobHelper?.loadNativeAd(unit_ad, AdMobNativeDisplaySize.MEDIUM)
        adMobHelper?.loadAdView(unitAdView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAds()
        initViews()
    }

    private fun loadNativeAd() {
        unit_ad.visibility = View.VISIBLE
        var textView = TextView(this@AdMobActivity)
        textView.setBackgroundColor(Color.parseColor("#CBCBCB"))
        textView.setPadding(10, 10, 10, 10)
        textView.gravity = Gravity.CENTER
        textView.text = "No Ads"
        textView.isAllCaps = true
        textView.textSize = 15f
        textView.setTypeface(null, Typeface.ITALIC)
        textView.setShadowLayer(2f, -1f, -1f, Color.DKGRAY)
        textView.setTextColor(Color.BLACK)
        var ivParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dimension(50).toInt())
        textView.layoutParams = ivParams
        unit_ad.removeAllViews()
        unit_ad.addView(textView)
        adMobHelper?.loadNativeAd(unit_ad, object : AdMobNativeListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                AdMobLogDebug.d("LOAD_NATIVE_AD", "AD Loaded Success!")
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                unit_ad.removeAllViews()
                unit_ad.addView(textView)
                AdMobLogDebug.d("LOAD_NATIVE_AD", "Failed to load native ad: $errorCode")
            }
        })
        return
    }

    override fun onPause() {
        super.onPause()
        adMobHelper?.pause()
    }

    override fun onResume() {
        super.onResume()
        adMobHelper?.resume()
    }

    override fun onDestroy() {
        adMobHelper?.destroy()
        super.onDestroy()
    }
}
