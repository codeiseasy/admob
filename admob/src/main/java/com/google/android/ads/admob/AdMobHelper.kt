package com.google.android.ads.admob

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView

import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import com.google.android.ads.R
import com.google.android.ads.admob.dialog.AdMobDialogLoading
import com.google.android.ads.admob.listener.AdMobListener
import com.google.android.ads.admob.listener.AdMobRewardedListener
import com.google.android.ads.admob.listener.AdMobNativeListener
import com.google.android.ads.admob.preference.AdMobPreferences
import com.google.android.ads.admob.templates.AdMobNativeDisplaySize
import com.google.android.ads.admob.templates.AdMobNativeTemplateStyle
import com.google.android.ads.admob.enums.AdMobSize
import com.google.android.ads.admob.util.AdMobCheckNetwork
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.ads.admob.formats.AdMobNativeView
import com.google.android.ads.consent.UserConsentSdk
import com.google.android.gms.ads.*
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


open class AdMobHelper(private val context: Context?, private val adRequest: AdRequest) {
    private var adMobPreferences: AdMobPreferences? = null
    private var isDebugTestAds: Boolean = false

    private var isEnableAds: Boolean = true
    private var isEnableBannerAd: Boolean = true
    private var isEnableInterstitialAd: Boolean = true
    private var isEnableRewardedVideoAd: Boolean = true
    private var isLoadAnimationBeforeLoadAd: Boolean = false
    private var animationAccentColor: Int = android.R.color.holo_orange_dark
    private var animationBeforeLoadAdHeight: Int = 100

    private var adAppUnitId: String? = null
    private var adViewUnitId: String? = null
    private var adInterstitialUnitId: String? = null
    private var adRewardedVideoUnitId: String? = null
    private var adNativeAdvancedUnitId: String? = null
    private var adNativeAdvancedVideoUnitId: String? = null

    private var interstitialAd: InterstitialAd? = null
    private var rewardedVideoAd: RewardedVideoAd? = null
    private var adViewBanner: AdView? = null
    private var adMobNativeView: AdMobNativeView? = null
    private var adBannerSize: AdSize? = null

    private var nativeTemplateStyle: AdMobNativeTemplateStyle? = null


    private var layoutInflater: LayoutInflater? = null
    private var displayMetrics: DisplayMetrics? = null
    private var adMobDialogLoading: AdMobDialogLoading? = null

    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            displayMetrics
        )
    }

    fun build() : AdMobHelper{
        init()
        return this
    }

    private fun init() {
        if (adRequest == null) {
            throw IllegalStateException("adRequest $initError")
        }
        this.adMobPreferences =
            AdMobPreferences(context)

        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(context) {}

        this.interstitialAd = InterstitialAd(context)

        this.rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)

        this.adViewBanner = AdView(context)

        //this.customAdView = CustomAdView(context)


        this.layoutInflater = LayoutInflater.from(context)
        this.displayMetrics = context!!.resources.displayMetrics


        var loadingView = layoutInflater!!.inflate(R.layout.gnt_loading_layout, null)
        var avLoadingIndicatorView = loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        avLoadingIndicatorView?.setIndicatorColor(
            ContextCompat.getColor(this.context!!, animationAccentColor)
        )
        avLoadingIndicatorView?.setIndicator(loadingAnimationType[Random().nextInt(loadingAnimationType.size)])

        var linearLayout = LinearLayout(context)
        var textView = TextView(context)

        var linearParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearParams.gravity = Gravity.CENTER
        linearLayout.layoutParams = linearParams
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER

        var textParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        textParams.topMargin = 10
        textView.layoutParams = textParams
        textView.text = "Loading Ad..."
        textView.setTextColor(Color.WHITE)
        textView.setShadowLayer(2f, -1f, -1f, Color.GRAY)
        textView.textSize = 18f
        textView.isAllCaps = true

        linearLayout.addView(loadingView)
        linearLayout.addView(textView)

        this.adMobDialogLoading = AdMobDialogLoading.init(context)
            .cancelable(false)
            .backgroundColor(android.R.color.white)
            .view(linearLayout)
            .build()
    }

    fun setAppUnitId(adUnitId: String?) {
        this.adAppUnitId = if (this.isDebugTestAds) {
            adTestAppUnitId
        } else {adUnitId}
        if (this.adAppUnitId == null) {
            throw IllegalStateException("app unit id $initError")
        }
    }

    fun setAdViewUnitId(adUnitId: String?) {
        this.adViewUnitId = if (this.isDebugTestAds) { adTestBannerId } else {adUnitId}
        this.adViewBanner?.adUnitId = this.adViewUnitId
    }

    fun setInterstitialUnitId(adUnitId: String?) {
        this.adInterstitialUnitId = if (this.isDebugTestAds) { adTestInterstitialId } else {adUnitId}
        this.interstitialAd?.adUnitId = this.adInterstitialUnitId
    }

    fun setRewardedVideoUnitId(adUnitId: String?) {
        this.adRewardedVideoUnitId = if (this.isDebugTestAds) { adTestRewardedVideoId } else {adUnitId}
    }

    fun setNativeAdvancedUnitId(adUnitId: String?) {
        this.adNativeAdvancedUnitId = if (this.isDebugTestAds) { adTestNativeAdvancedId } else {adUnitId}
    }

    fun setNativeAdvancedVideoUnitId(adUnitId: String?) {
        this.adNativeAdvancedVideoUnitId = if (this.isDebugTestAds) { adTestNativeAdvancedVideoId } else {adUnitId}
    }

    fun setAdViewSize(adSize: AdSize) {
        this.adBannerSize = adSize
    }

    fun setLoadAnimationBeforeLoadAd(anim: Boolean) {
        this.isLoadAnimationBeforeLoadAd = anim
    }

    fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int) {
        this.animationAccentColor = color
    }

    fun setLoadAnimationBeforeLoadAdHeight(size: Int) {
        this.animationBeforeLoadAdHeight = size
    }

    open fun loadAdView(adView: AdView) {
        this.loadAdView(adView, null)
    }

    open fun loadAdView(adView: AdView, listener: AdMobListener?) {
        if (isEnableAds){
            if (isEnableBannerAd){
                this.loadAdViewIfEnabled(adView, listener)
            } else {
                listener?.onAdClosed()
            }
        } else {
            if (isEnableBannerAd){
                this.loadAdViewIfEnabled(adView, listener)
            } else {
                listener?.onAdClosed()
            }
        }
    }

    private fun loadAdViewIfEnabled(adView: AdView, listener: AdMobListener?) {
        this.adViewBanner = adView
        this.adViewBanner?.loadAd(this.adRequest)
        this.bannerAdListener(listener)
    }

    open fun loadAdView(layoutAd: RelativeLayout) {
        this.loadAdView(layoutAd, null)
    }

    open fun loadAdView(layoutAd: ViewGroup, listener: AdMobListener?) {
        if (this.adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (this.adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        if (isEnableAds){
            if (isEnableBannerAd){
                this.loadAdViewIfEnabled(layoutAd, listener)
            } else {
                listener?.onAdClosed()
            }
        } else {
            if (isEnableBannerAd){
                this.loadAdViewIfEnabled(layoutAd, listener)
            } else {
                listener?.onAdClosed()
            }
        }
    }

    private fun loadAdViewIfEnabled(layoutAd: ViewGroup, listener: AdMobListener?) {
        var adContainer = layoutAd
        this.adViewBanner = AdView(this.context)

        val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        this.adViewBanner?.layoutParams = params

        this.adViewBanner?.id = R.id.gnt_view_banner_id
        this.adViewBanner?.adSize = this.adBannerSize
        this.adViewBanner?.adUnitId = this.adViewUnitId
        adContainer.removeView(this.adViewBanner)
        adContainer.addView(this.adViewBanner)
        this.adViewBanner?.loadAd(this.adRequest)
        this.bannerAdListener(listener)
    }

    open fun loadInterstitialAd() {
        this.loadInterstitialAd(null)
    }

    open fun loadInterstitialAd(listener: AdMobListener?) {
        if (this.adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        if (isEnableAds){
            if (isEnableInterstitialAd){
                loadInterstitialAdIfEnabled(listener)
            } else {
                listener?.onAdClosed()
            }
        } else {
            if (isEnableInterstitialAd){
                loadInterstitialAdIfEnabled(listener)
            } else {
                listener?.onAdClosed()
            }
        }
    }

    private fun loadInterstitialAdIfEnabled(listener: AdMobListener?) {
        this.interstitialAdListener(listener)
    }


    open fun loadAdRewardedVideo() {
        this.loadAdRewardedVideo(null)
    }

    open fun loadAdRewardedVideo(listenerMob: AdMobRewardedListener?) {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        if (isEnableAds){
            if (isEnableRewardedVideoAd){
                this.loadAdRewardedVideoIfEnabled(listenerMob)
            } else {
                listenerMob?.onRewardedVideoAdClosed()
            }
        } else {
            if (isEnableRewardedVideoAd){
                this.loadAdRewardedVideoIfEnabled(listenerMob)
            } else {
                listenerMob?.onRewardedVideoAdClosed()
            }
        }
    }

    private fun loadAdRewardedVideoIfEnabled(listenerMob: AdMobRewardedListener?) {
        this.rewardedVideoListener(listenerMob)
    }

    open fun loadInterstitialAfterEndCount(view: View?, count: Int) {
        var key = view!!.id.toString()
        this.loadInterstitialAfterEndCount(key, count)
    }

    open fun loadInterstitialAfterEndCount(key: String?, count: Int) {
        this.loadInterstitialAfterEndCount(key, count, null)
    }

    open fun loadInterstitialAfterEndCount(view: View?, count: Int, listener: AdMobListener?) {
        var key = view!!.id.toString()
        this.loadInterstitialAfterEndCount(key, count, listener)
    }

    open fun loadInterstitialAfterEndCount(key: String?, count: Int, listener: AdMobListener?) {
        Log.d("ADMOB", "AdMobHelper init(${this.adInterstitialUnitId})")
        if (this.adInterstitialUnitId.isNullOrEmpty()) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        if (isEnableAds){
            if (isEnableInterstitialAd){
                loadInterstitialAfterEndCountIfEnabled(key, count, listener)
            } else {
                listener?.onAdClosed()
            }
        } else {
            if (isEnableInterstitialAd){
                loadInterstitialAfterEndCountIfEnabled(key, count, listener)
            } else {
                listener?.onAdClosed()
            }
        }
    }

    private fun loadInterstitialAfterEndCountIfEnabled(key: String?, count: Int, listener: AdMobListener?){
        var totalCount = doCountInterstitialAd(key)
        if (AdMobCheckNetwork.getInstance(this.context).isOnline) {
            if (this.adMobPreferences!!.getAdCountPref(key) == count) {
                this.adMobPreferences?.setAdCountPref(key, 1)
                this.interstitialAdListener(listener)
            } else {
                this.adMobPreferences?.setAdCountPref(key, totalCount)
                listener?.onAdClosed()
            }
        } else {
            listener?.onAdClosed()
        }
        Log.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }


    open fun loadRewardedVideoAfterEndCount(view: View?, count: Int) {
        var key = view!!.id.toString()
        this.loadRewardedVideoAfterEndCount(key, count)
    }

    open fun loadRewardedVideoAfterEndCount(key: String, count: Int) {
       this.loadRewardedVideoAfterEndCount(key, count, null)
    }

    open fun loadRewardedVideoAfterEndCount(view: View?, count: Int, listenerMob: AdMobRewardedListener?) {
        var key = view!!.id.toString()
        this.loadRewardedVideoAfterEndCount(key, count, listenerMob)
    }

    open fun loadRewardedVideoAfterEndCount(key: String, count: Int, listenerMob: AdMobRewardedListener?) {
        if (this.adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        if (isEnableAds){
           if (isEnableRewardedVideoAd){
               loadRewardedVideoAfterEndCountIfEnabled(key, count, listenerMob)
           } else {
               listenerMob?.onRewardedVideoAdClosed()
           }
        } else {
            if (isEnableRewardedVideoAd){
                loadRewardedVideoAfterEndCountIfEnabled(key, count, listenerMob)
            } else {
                listenerMob?.onRewardedVideoAdClosed()
            }
        }
    }

    private fun loadRewardedVideoAfterEndCountIfEnabled(key: String, count: Int, listenerMob: AdMobRewardedListener?) {
        var totalCount = doCountRewardedVideo(key)
        if (AdMobCheckNetwork.getInstance(this.context).isOnline) {
            if (this.adMobPreferences!!.getAdCountPref(key) == count) {
                this.adMobPreferences?.setAdCountPref(key, 1)
                this.rewardedVideoListener(listenerMob)
            } else {
                this.adMobPreferences?.setAdCountPref(key, totalCount)
                listenerMob?.onRewardedVideoAdClosed()
            }
        } else {
            listenerMob?.onRewardedVideoAdClosed()
        }
        AdMobLogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun doCountInterstitialAd(key: String?): Int {
        return this.adMobPreferences!!.getAdCountPref(key) + 1
    }

    private fun doCountRewardedVideo(key: String?): Int {
        return this.adMobPreferences!!.getAdCountPref(key) + 1
    }

    private fun bannerAdListener(listener: AdMobListener?) {
        this.adViewBanner?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                listener?.onAdLoaded()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdLoaded()}]")

            }

            override fun onAdOpened() {
                super.onAdOpened()
                listener?.onAdOpened()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdOpened()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                listener?.onAdClicked()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdClicked()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                listener?.onAdClosed()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdClosed()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                listener?.onAdImpression()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdImpression()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                listener?.onAdLeftApplication()
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdLeftApplication()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                listener?.onAdFailedToLoad(i)
                AdMobLogDebug.d(TAG, "[loadCustomAdView(){\nonAdFailedToLoad($i)}]")
            }
        }
    }

    private fun interstitialAdListener(listener: AdMobListener?) {
        if (isLoadAnimationBeforeLoadAd) {
            this.adMobDialogLoading?.show()
        }
        this.interstitialAd?.adUnitId = adInterstitialUnitId
        this.interstitialAd?.loadAd(this.adRequest)
        this.interstitialAd?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                if (isLoadAnimationBeforeLoadAd) {
                    adMobDialogLoading?.hide()
                }
                listener?.onAdLoaded()
                if (interstitialAd!!.isLoaded) {
                    interstitialAd?.show()
                }
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLoaded()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                listener?.onAdClicked()
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClicked()}]")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                listener?.onAdOpened()
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdOpened()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                listener?.onAdClosed()
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClosed()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                listener?.onAdLeftApplication()
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLeftApplication()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                listener?.onAdImpression()
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdImpression()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                if (isLoadAnimationBeforeLoadAd) {
                    adMobDialogLoading?.hide()
                }
                listener?.onAdFailedToLoad(i)
                AdMobLogDebug.d(TAG, "[loadInterstitialAd(){\nonAdFailedToLoad($i)}]")
            }
        }
    }

    private fun rewardedVideoListener(listenerMob: AdMobRewardedListener?) {
        if (isLoadAnimationBeforeLoadAd) {
            this.adMobDialogLoading?.show()
        }
        this.rewardedVideoAd?.loadAd(this.adRewardedVideoUnitId, this.adRequest)
        this.rewardedVideoAd?.rewardedVideoAdListener = object : RewardedVideoAdListener {

            override fun onRewardedVideoAdLoaded() {
                listenerMob?.onRewardedVideoAdLoaded()
                if (isLoadAnimationBeforeLoadAd) {
                    adMobDialogLoading?.hide()
                }
                if (this@AdMobHelper.rewardedVideoAd!!.isLoaded) {
                    this@AdMobHelper.rewardedVideoAd?.show()
                }
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLoaded()}]")
            }

            override fun onRewardedVideoAdOpened() {
                listenerMob?.onRewardedVideoAdOpened()
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdOpened()}]")
            }

            override fun onRewardedVideoStarted() {
                listenerMob?.onRewardedVideoStarted()
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoStarted()}]")
            }

            override fun onRewardedVideoAdClosed() {
                listenerMob?.onRewardedVideoAdClosed()
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdClosed()}]")
            }

            override fun onRewarded(rewardItem: RewardItem) {
                listenerMob?.onRewarded(rewardItem)
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewarded(${rewardItem.type + " : " + rewardItem.amount})}]")
            }

            override fun onRewardedVideoAdLeftApplication() {
                listenerMob?.onRewardedVideoAdLeftApplication()
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLeftApplication()}]")
            }

            override fun onRewardedVideoAdFailedToLoad(i: Int) {
                listenerMob?.onRewardedVideoAdFailedToLoad(i)
                if (isLoadAnimationBeforeLoadAd) {
                    adMobDialogLoading?.hide()
                }
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdFailedToLoad($i)}]")
            }

            override fun onRewardedVideoCompleted() {
                listenerMob?.onRewardedVideoCompleted()
                AdMobLogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoCompleted()}]")
            }
        }
    }

    fun loadNativeAd(unitAdView: ViewGroup) {
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, null)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adMobNativeListener: AdMobNativeListener?) {
        this.loadUnifiedNativeAd(unitAdView, AdMobNativeDisplaySize.MEDIUM, adMobNativeListener)
    }

    fun loadNativeAd(unitAdView: ViewGroup, displaySize: AdMobNativeDisplaySize) {
        this.loadUnifiedNativeAd(unitAdView, displaySize, null)
    }

    fun loadNativeAd(unitAdView: ViewGroup, displaySize: AdMobNativeDisplaySize, adMobNativeListener: AdMobNativeListener?) {
        this.loadUnifiedNativeAd(unitAdView, displaySize, adMobNativeListener)
    }

    private fun loadUnifiedNativeAd(unitAdView: ViewGroup, displaySize: AdMobNativeDisplaySize, adMobNativeListener: AdMobNativeListener?) {
        when (displaySize) {
            AdMobNativeDisplaySize.SMALL -> this.refreshNativeAd(
                unitAdView,
                R.layout.gnt_small_template_view, // ad_small_unified_native
                adMobNativeListener
            )
            AdMobNativeDisplaySize.MEDIUM -> this.refreshNativeAd(
                unitAdView,
                R.layout.gnt_medium_template_view, // ad_medium_unified_native
                adMobNativeListener
            )
            AdMobNativeDisplaySize.LARGE -> this.refreshNativeAd(
                unitAdView,
                R.layout.gnt_large_template_view,
                adMobNativeListener
            )
        }
    }

    private fun refreshNativeAd(unitAd: ViewGroup, @LayoutRes layoutRes: Int, adMobNativeListener: AdMobNativeListener?) {
        if (this.adNativeAdvancedUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on NativeAdvancedUnitId before loadAd is called.")
        }
        if (isEnableAds){
            var colorDrawable = ColorDrawable(Color.parseColor("#FFFFFF"))
            if (nativeTemplateStyle == null){
                this.nativeTemplateStyle = AdMobNativeTemplateStyle.Builder()
                    .withMainBackgroundColor(colorDrawable)
                    .build()
            }
            val adMobNativeView: AdMobNativeView = unitAd as AdMobNativeView
            adMobNativeView.adUnitId = adNativeAdvancedUnitId
            adMobNativeView.templateRes = layoutRes
            adMobNativeView.templateStyle = nativeTemplateStyle
            adMobNativeView.loadAd(adRequest)
            adMobNativeView.adMobNativeListener = object : AdMobNativeListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adMobNativeListener?.onAdLoaded()
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    adMobNativeListener?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    adMobNativeListener?.onAdClosed()
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    super.onAdFailedToLoad(errorCode)
                    adMobNativeListener?.onAdFailedToLoad(errorCode)
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    adMobNativeListener?.onAdClicked()
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    adMobNativeListener?.onAdImpression()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    adMobNativeListener?.onAdLeftApplication()
                }
            }
        }
    }

    // onResume Ad
    fun resume() {
        this.adViewBanner?.resume()
    }

    // ooPause Ad
    fun pause() {
        this.adViewBanner?.pause()
    }

    // onDestroy Ad
    fun destroy() {
        this.adViewBanner?.destroy()
        this.adMobNativeView?.destroyNativeAd()
    }

    //TODO  Builder class
    class Builder {
        private var context: Context? = null
        private var adSize: AdSize = AdSize.BANNER
        private var appUnitId: String? = null
        private var adViewUnitId: String? = null
        private var interstitialUnitId: String? = null
        private var rewardedVideoUnitId: String? = null
        private var nativeAdvancedUnitId: String? = null
        private var animationAccentColor: Int = android.R.color.holo_orange_dark
        private var animationBeforeLoadAdHeight: Int = 100
        private var adRequest: AdRequest? = null
        private var isDebugTestAds: Boolean = false
        private var isEnableAds: Boolean = true
        private var isEnableBannerAd: Boolean = true
        private var isEnableInterstitialAd: Boolean = true
        private var isEnableRewardedVideoAd: Boolean = true
        private var isLoadAnimationBeforeLoadAd: Boolean = false
        private var nativeAdvancedTemplateStyles: AdMobNativeTemplateStyle? = null

        fun setContext(context: Context?): Builder {
            this.context = context
            return this
        }

        fun setAppUnitId(unitId: String): Builder {
            this.appUnitId = unitId
            return this
        }

        fun setAdViewUnitId(unitId: String): Builder {
            this.adViewUnitId = unitId
            return this
        }

        fun setInterstitialUnitId(unitId: String): Builder {
            this.interstitialUnitId = unitId
            return this
        }

        fun setRewardedVideoUnitId(unitId: String): Builder {
            this.rewardedVideoUnitId = unitId
            return this
        }

        fun setNativeAdvancedUnitId(unitId: String): Builder {
            this.nativeAdvancedUnitId = unitId
            return this
        }

        fun setAdViewSize(size: AdMobSize): Builder {
            checkAdSize(size)
            return this
        }

        private fun checkAdSize(size: AdMobSize){
            this.adSize = when(size){
                AdMobSize.BANNER -> AdSize.BANNER
                AdMobSize.SMART_BANNER -> AdSize.SMART_BANNER
                AdMobSize.LARGE_BANNER -> AdSize.LARGE_BANNER
                AdMobSize.FULL_BANNER -> AdSize.FULL_BANNER
                AdMobSize.MEDIUM_RECTANGLE -> AdSize.MEDIUM_RECTANGLE
                AdMobSize.LEADERBOARD -> AdSize.LEADERBOARD
                AdMobSize.WIDE_SKYSCRAPER -> AdSize.WIDE_SKYSCRAPER
                AdMobSize.FLUID -> AdSize.FLUID
                AdMobSize.INVALID -> AdSize.INVALID
                AdMobSize.SEARCH -> AdSize.SEARCH
            }
        }

        private fun setAdRequest(request: AdRequest?): Builder {
            this.adRequest = request
            return this
        }

        fun setDebugTestAds(debug: Boolean): Builder {
            this.isDebugTestAds = debug
            return this
        }

        fun setLoadAnimationBeforeLoadAd(anim: Boolean): Builder {
            this.isLoadAnimationBeforeLoadAd = anim
            return this
        }

        fun setLoadAnimationBeforeLoadAdAccentColor(@ColorRes color: Int): Builder {
            this.animationAccentColor = color
            return this
        }

        fun setLoadAnimationBeforeLoadAdHeight(size: Int): Builder {
            this.animationBeforeLoadAdHeight = size
            return this
        }

        fun setEnableAds(enable: Boolean): Builder{
            this.isEnableAds = enable
            return this
        }

        fun setEnableBannerAd(enable: Boolean): Builder{
            this.isEnableBannerAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setEnableInterstitialAd(enable: Boolean): Builder{
            this.isEnableInterstitialAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setEnableRewardedVideoAd(enable: Boolean): Builder{
            this.isEnableRewardedVideoAd = enable
            this.isEnableAds = enable
            return this
        }

        fun setNativeAdvancedStyles(styles: AdMobNativeTemplateStyle): Builder {
            this.nativeAdvancedTemplateStyles = styles
            return this
        }

        fun build(): AdMobHelper {
            if (adRequest == null) {
                adRequest = UserConsentSdk.getInstance(this.context!!).getAdMobRequest()
            }
            var advertiseHandler = AdMobHelper(this.context, adRequest!!)
            advertiseHandler.setAppUnitId(this.appUnitId)
            advertiseHandler.setAdViewUnitId(this.adViewUnitId)
            advertiseHandler.setInterstitialUnitId(this.interstitialUnitId)
            advertiseHandler.setRewardedVideoUnitId(this.rewardedVideoUnitId)
            advertiseHandler.setNativeAdvancedUnitId(this.nativeAdvancedUnitId)
            advertiseHandler.setAdViewSize(this.adSize)
            advertiseHandler.setLoadAnimationBeforeLoadAd(this.isLoadAnimationBeforeLoadAd)
            advertiseHandler.setLoadAnimationBeforeLoadAdAccentColor(this.animationAccentColor)
            advertiseHandler.setLoadAnimationBeforeLoadAdHeight(this.animationBeforeLoadAdHeight)
            advertiseHandler.isDebugTestAds = this.isDebugTestAds
            advertiseHandler.isEnableAds = this.isEnableAds
            advertiseHandler.isEnableBannerAd = this.isEnableBannerAd
            advertiseHandler.isEnableInterstitialAd = this.isEnableInterstitialAd
            advertiseHandler.isEnableRewardedVideoAd = this.isEnableRewardedVideoAd
            advertiseHandler.nativeTemplateStyle = this.nativeAdvancedTemplateStyles
            advertiseHandler.build()
            return advertiseHandler
        }
    }

    companion object {
        private var TAG = AdMobHelper::class.java.name
        private const val initError = "is not initialized yet"
        private const val adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713"
        private const val adTestBannerId = "ca-app-pub-3940256099942544/6300978111"
        private const val adTestInterstitialId = "ca-app-pub-3940256099942544/1033173712"
        private const val adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917"
        private const val adTestNativeAdvancedId = "ca-app-pub-3940256099942544/2247696110"
        private const val adTestNativeAdvancedVideoId = "ca-app-pub-3940256099942544/1044960115"

        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )

        var adMobHelper: AdMobHelper? = null

        @Synchronized
        fun getInstance(context: Context?, adRequest: AdRequest): AdMobHelper? {
            synchronized(AdMobHelper::class.java) {
                if (adMobHelper == null)
                    return AdMobHelper(context, adRequest)
            }
            return adMobHelper
        }
    }

}