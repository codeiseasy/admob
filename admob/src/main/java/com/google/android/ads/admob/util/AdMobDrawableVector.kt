package com.google.android.ads.admob.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Build
import android.util.DisplayMetrics
import android.util.TypedValue

import android.util.TypedValue.COMPLEX_UNIT_DIP

class AdMobDrawableVector(context: Context?) {
    private val displayMetrics: DisplayMetrics = context!!.resources.displayMetrics
    private val padding = IntArray(4)
    private var radius: Int? = 0
    private var radiusTop = 0
    private var radiusBottom = 0
    private var isMultipleCornerRadius: Boolean? = false
    private var isMultipleColors: Boolean? = false
    private var bgColor: Int = Color.WHITE
    private var bgColors: IntArray = intArrayOf(Color.WHITE)

    private fun dimension(value: Float): Int {
        return TypedValue.applyDimension(COMPLEX_UNIT_DIP, value, displayMetrics).toInt()
    }

    fun padding(all: Int): AdMobDrawableVector {
        this.padding[0] = all
        this.padding[1] = all
        this.padding[2] = all
        this.padding[3] = all
        return this
    }

    fun padding(left: Int, top: Int, right: Int, bottom: Int): AdMobDrawableVector {
        this.padding[0] = left
        this.padding[1] = top
        this.padding[2] = right
        this.padding[3] = bottom
        return this
    }

    fun backgroundColor(color: Int): AdMobDrawableVector {
        this.bgColor = color
        this.isMultipleColors = false
        return this
    }

    fun backgroundColors(colors: IntArray?): AdMobDrawableVector {
        this.bgColors = colors!!
        this.isMultipleColors = true
        return this
    }


    fun cornerRadius(radius: Int?): AdMobDrawableVector {
        this.radius = radius
        this.isMultipleCornerRadius = false
        return this
    }


    fun cornerRadiusTop(radius: Int?): AdMobDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusTop = radius!!
        return this
    }

    fun cornerRadiusBottom(radius: Int?): AdMobDrawableVector {
        this.isMultipleCornerRadius = true
        this.radiusBottom = radius!!
        return this
    }

    fun apply(): Drawable {
        val gradientDrawable = GradientDrawable()
        gradientDrawable.gradientType = GradientDrawable.RADIAL_GRADIENT
        gradientDrawable.orientation = GradientDrawable.Orientation.RIGHT_LEFT
        if (isMultipleColors!!){
            gradientDrawable.colors = bgColors
        } else {
            gradientDrawable.setColor(bgColor)
        }
        gradientDrawable.setStroke(dimension(0.3f), Color.parseColor("#3B4045"))
        if (!isMultipleCornerRadius!!){
            //gradientDrawable.cornerRadius = dimension(radius.toFloat()).toFloat()
            gradientDrawable.cornerRadii = floatArrayOf(
                    // top of section
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat(),

                    //Bottom of section
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat(),
                    dimension(radius!!.toFloat()).toFloat()
            )
        } else {
            gradientDrawable.cornerRadii = floatArrayOf(
                    // top of section
                    dimension(radiusTop.toFloat()).toFloat(),
                    dimension(radiusTop.toFloat()).toFloat(),
                    dimension(radiusTop.toFloat()).toFloat(),
                    dimension(radiusTop.toFloat()).toFloat(),

                    //Bottom of section
                    dimension(radiusBottom.toFloat()).toFloat(),
                    dimension(radiusBottom.toFloat()).toFloat(),
                    dimension(radiusBottom.toFloat()).toFloat(),
                    dimension(radiusBottom.toFloat()).toFloat()
            )
        }
        val drawables = arrayOf<Drawable>(gradientDrawable)
        val layerDrawable = LayerDrawable(drawables)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            layerDrawable.setPadding(
                dimension(padding[0].toFloat()),
                dimension(padding[1].toFloat()),
                dimension(padding[2].toFloat()),
                dimension(padding[3].toFloat())
            )
        }
        layerDrawable.setLayerInset(0, 0, 0, 0, 0)
        return layerDrawable
    }



    companion object {

        fun setupShape(context: Context?): AdMobDrawableVector {
            return AdMobDrawableVector(context)
        }

        fun setupBorderDrawable(): BorderDrawable {
            return BorderDrawable()
        }
    }


     class BorderDrawable {
        private var radius: Float = 0f
        private var backgroundColor: Int = Color.TRANSPARENT
        private var strokeWidth: Int = 1
        private var strokeColor: Int = Color.TRANSPARENT

        fun setStroke(width: Int, color: Int): BorderDrawable {
            this.strokeWidth = width
            this.strokeColor = color
            return this
        }

        fun setStrokeWidth(stroke: Int): BorderDrawable {
            this.strokeWidth = stroke
            return this
        }

        fun setStrokeColor(color: Int): BorderDrawable {
            this.strokeColor = color
            return this
        }

        fun setCornerRadius(radius: Float): BorderDrawable {
            this.radius = radius
            return this
        }

        fun setBachgroundColor(color: Int): BorderDrawable {
            this.backgroundColor = color
            return this
        }

        fun apply(): Drawable{
            val border = GradientDrawable()
            border.setColor(backgroundColor)
            border.cornerRadius = radius
            border.setStroke(strokeWidth, strokeColor)
            return border
        }
    }
}
