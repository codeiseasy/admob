package com.google.android.ads.admob.formats

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.google.android.ads.R
import com.google.android.ads.admob.listener.AdMobNativeListener
import com.google.android.ads.admob.templates.AdMobNativeTemplateStyle
import com.google.android.ads.admob.util.AdMobLogDebug
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.VideoOptions
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.wang.avi.AVLoadingIndicatorView
import java.util.*

/**
 * Author: Med Ajaroud
 * Created At: 06/02/2020
 * Github: https://github.com/mrajaroud
 * License: Non-commercial
 */

/** Base class for a native ad template view. *  */
class AdMobNativeView : RelativeLayout {
    var templateRes = 0
    var adUnitId: String? = null
    var adMobNativeListener: AdMobNativeListener? = null
    var templateStyle: AdMobNativeTemplateStyle? = null

    private var isLoadAnimationBeforeLoadAd: Boolean = false
    private var animationAccentColor = 0


    private var nativeAd: UnifiedNativeAd? = null
    private var nativeAdView: UnifiedNativeAdView? = null
    private var primaryView: TextView? = null
    private var secondaryView: TextView? = null
    private var ratingBar: RatingBar? = null
    private var tertiaryView: TextView? = null
    private var badgeView: TextView? = null
    private var iconView: ImageView? = null
    private var mediaView: MediaView? = null
    private var callToActionView: Button? = null
    private var background: ConstraintLayout? = null
    private var inflater: LayoutInflater? = null
    private var inflaterView: View? = null
    private var avLoadingIndicatorView: AVLoadingIndicatorView? = null
    private var adLoaderBuilder: AdLoader.Builder? = null


    constructor(context: Context?, @LayoutRes layoutRes: Int) : super(context) {
        this.templateRes = layoutRes
        this.initView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this.initView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        this.initView(context, attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        this.initView(context, attrs)
    }

    private fun initView(context: Context?, attributeSet: AttributeSet?) {
        this.inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val attributes = context.theme.obtainStyledAttributes(attributeSet, R.styleable.TemplateView, 0, 0)
        try {
            this.templateRes = attributes.getResourceId(
                R.styleable.TemplateView_gnt_template_type,
                R.layout.gnt_medium_template_view
            )
            this.isLoadAnimationBeforeLoadAd = attributes.getBoolean(R.styleable.TemplateView_gnt_animation, true)
            this.animationAccentColor = attributes.getColor(R.styleable.TemplateView_gnt_animation_accent_color, ContextCompat.getColor(context, R.color.user_consent_accent_color))
        } finally {
            attributes.recycle()
        }

        if (this.isLoadAnimationBeforeLoadAd) {
            this.inflater?.inflate(R.layout.gnt_loading_layout, this, true)
            this.avLoadingIndicatorView = findViewById(R.id.ad_loading_view)
            if (this.avLoadingIndicatorView != null) {
                this.avLoadingIndicatorView?.setIndicatorColor(animationAccentColor)
                this.avLoadingIndicatorView?.setIndicator(
                    loadingAnimationType[Random().nextInt(
                        loadingAnimationType.size
                    )]
                )
            }
        }
    }

    private fun inflateViews() {
        this.inflaterView = this.inflater?.inflate(templateRes, this, true)
        this.nativeAdView = findViewById(R.id.unified_native_ad_view)
        this.primaryView = findViewById(R.id.tv_primary)
        this.secondaryView = findViewById(R.id.tv_secondary)
        this.tertiaryView = findViewById(R.id.tv_body)
        this.badgeView = findViewById(R.id.tv_ad_badge)
        this.ratingBar = findViewById(R.id.rating_bar)
        this.ratingBar?.isEnabled = false
        this.callToActionView = findViewById(R.id.btn_call_to_action)
        this.iconView = findViewById(R.id.iv_icon)
        this.mediaView = findViewById(R.id.media_view)
        this.background = findViewById(R.id.background)
    }

    private fun setStyles(styles: AdMobNativeTemplateStyle?) {
        this.templateStyle = styles
        this.applyStyles()
    }

    private fun applyStyles() {
        val mainBackground: Drawable? = templateStyle!!.mainBackgroundColor
        if (mainBackground != null) {
            this.background?.background = mainBackground
            if (this.primaryView != null) {
                //primaryView?.background = mainBackground
            }
            if (this.secondaryView != null) {
               // secondaryView?.background = mainBackground
            }
            if (this.tertiaryView != null) {
                //tertiaryView?.background = mainBackground
            }
        }
        val primary = this.templateStyle!!.primaryTextTypeface
        if (primary != null && this.primaryView != null) {
            this.primaryView?.typeface = primary
        }
        val secondary = this.templateStyle!!.secondaryTextTypeface
        if (secondary != null && this.secondaryView != null) {
            this.secondaryView?.typeface = secondary
        }
        val tertiary = this.templateStyle!!.tertiaryTextTypeface
        if (tertiary != null && this.tertiaryView != null) {
            this.tertiaryView?.typeface = tertiary
        }
        val ctaTypeface = this.templateStyle!!.callToActionTextTypeface
        if (ctaTypeface != null && this.callToActionView != null) {
            this.callToActionView?.typeface = ctaTypeface
        }
        val primaryTypefaceColor = this.templateStyle!!.primaryTextTypefaceColor
        if (primaryTypefaceColor > 0 && this.primaryView != null) {
            this.primaryView?.setTextColor(primaryTypefaceColor)
        }
        val secondaryTypefaceColor = this.templateStyle!!.secondaryTextTypefaceColor
        if (secondaryTypefaceColor > 0 && this.secondaryView != null) {
            secondaryView?.setTextColor(secondaryTypefaceColor)
        }
        val tertiaryTypefaceColor = this.templateStyle!!.tertiaryTextTypefaceColor
        if (tertiaryTypefaceColor > 0 && this.tertiaryView != null) {
            this.tertiaryView?.setTextColor(tertiaryTypefaceColor)
        }
        val badgeTypeFace = this.templateStyle!!.badgeTextTypeface
        if (this.badgeView != null && badgeTypeFace != null){
            this.badgeView?.typeface = badgeTypeFace
        }

        val ctaTypefaceColor = this.templateStyle!!.callToActionTypefaceColor
        if (ctaTypefaceColor > 0 && this.callToActionView != null) {
            this.callToActionView?.setTextColor(ctaTypefaceColor)
        }
        val ctaTextSize = this.templateStyle!!.callToActionTextSize
        if (ctaTextSize > 0 && this.callToActionView != null) {
            this.callToActionView?.textSize = ctaTextSize
        }
        val primaryTextSize = this.templateStyle!!.primaryTextSize
        if (primaryTextSize > 0 && this.primaryView != null) {
            this.primaryView?.textSize = primaryTextSize
        }
        val secondaryTextSize = this.templateStyle!!.secondaryTextSize
        if (secondaryTextSize > 0 && this.secondaryView != null) {
            this.secondaryView?.textSize = secondaryTextSize
        }
        val tertiaryTextSize = this.templateStyle!!.tertiaryTextSize
        if (tertiaryTextSize > 0 && this.tertiaryView != null) {
            this.tertiaryView?.textSize = tertiaryTextSize
        }
        val ctaBackground: Drawable? = this.templateStyle!!.callToActionBackgroundColor
        if (ctaBackground != null && this.callToActionView != null) {
            this.callToActionView?.background = ctaBackground
        }
        val primaryBackground: Drawable? = this.templateStyle!!.primaryTextBackgroundColor
        if (primaryBackground != null && this.primaryView != null) {
            this.primaryView?.background = primaryBackground
        }
        val secondaryBackground: Drawable? = this.templateStyle!!.secondaryTextBackgroundColor
        if (secondaryBackground != null && this.secondaryView != null) {
            this.secondaryView?.background = secondaryBackground
        }
        val tertiaryBackground: Drawable? = this.templateStyle!!.tertiaryTextBackgroundColor
        if (tertiaryBackground != null && this.tertiaryView != null) {
            this.tertiaryView?.background = tertiaryBackground
        }
        if (this.mediaView != null){
            this.mediaView?.setImageScaleType(ImageView.ScaleType.CENTER_CROP)
            this.mediaView?.background = mainBackground
        }

        invalidate()
        requestLayout()
    }

    private fun adHasOnlyStore(nativeAd: UnifiedNativeAd): Boolean {
        val store = nativeAd.store
        val advertiser = nativeAd.advertiser
        return !TextUtils.isEmpty(store) && TextUtils.isEmpty(advertiser)
    }

    private fun setNativeAd(nativeAd: UnifiedNativeAd?) {
        //this.nativeAd = nativeAd
        val store = nativeAd!!.store
        val advertiser = nativeAd.advertiser
        val headline = nativeAd.headline
        val body = nativeAd.body
        val cta = nativeAd.callToAction
        val starRating = nativeAd.starRating
        val icon = nativeAd.icon
        Log.d("NATIVE_RESULT", advertiser)


        val secondaryText: String
        this.nativeAdView?.callToActionView = this.callToActionView
        this.nativeAdView?.headlineView = this.primaryView
        this.nativeAdView?.mediaView = this.mediaView
        this.secondaryView?.visibility = View.VISIBLE
        if (adHasOnlyStore(nativeAd)) {
            this.nativeAdView?.storeView = this.secondaryView
            secondaryText = store
        } else if (!TextUtils.isEmpty(advertiser)) {
            this.nativeAdView?.advertiserView = this.secondaryView
            secondaryText = advertiser
        } else {
            secondaryText = ""
        }
        this.primaryView?.text = headline
        this.callToActionView?.text = cta

        //  Set the secondary view to be the star rating if available.
        if (starRating != null && starRating > 0) {
            this.secondaryView?.visibility = View.GONE
            this.ratingBar?.visibility = View.VISIBLE
            this.ratingBar?.max = 5
            this.nativeAdView?.starRatingView = this.ratingBar
        } else {
            this.secondaryView?.text = secondaryText
            this.secondaryView?.visibility = View.VISIBLE
            this.ratingBar?.visibility = View.GONE
        }
        if (icon != null) {
            this.iconView?.visibility = View.VISIBLE
            this.iconView?.setImageDrawable(icon.drawable)
        } else {
            this.iconView?.visibility = View.GONE
        }
        if (this.tertiaryView != null) {
            this.tertiaryView?.text = body
            this.nativeAdView?.bodyView = this.tertiaryView
        }
        this.nativeAdView?.setNativeAd(nativeAd)
    }

    /**
     * To prevent memory leaks, make sure to destroy your ad when you don't need it anymore. This
     * method does not destroy the template view.
     * https://developers.google.com/admob/android/native-unified#destroy_ad
     */
    fun destroyNativeAd() {
        if (this.nativeAd != null){
            this.nativeAd?.destroy()
        }
    }

    val templateTypeName: String
        get() {
            return when (this.templateRes) {
                R.layout.gnt_small_template_view -> {
                    SMALL_TEMPLATE
                }
                R.layout.gnt_medium_template_view -> {
                    MEDIUM_TEMPLATE
                }
                R.layout.gnt_large_template_view -> {
                    LARGE_TEMPLATE
                }
                else -> ""
            }
        }


    fun setTemplateView(@LayoutRes layoutRes: Int) {
        this.templateRes = layoutRes
        this.inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.inflater?.inflate(this.templateRes, this, true)
    }


    fun loadAd(adRequest: AdRequest) {
        if (adRequest == null) {
            throw IllegalStateException("adRequest is not initialized yet")
        }
        if (TextUtils.isEmpty(this.adUnitId)) {
            throw IllegalStateException("The ad unit ID must be set on NativeAdvancedUnitId before loadAd is called.")
        }

        this.adLoaderBuilder = AdLoader.Builder(this.context, this.adUnitId)
        this.adLoaderBuilder?.forUnifiedNativeAd { unifiedNativeAd ->
            // Assumes that your ad layout is in a file call ad_unified.xml
            // in the res/layout folder
            AdMobLogDebug.d(TAG, "forUnifiedNativeAd($unifiedNativeAd)")
            this.nativeAd = unifiedNativeAd
        }
        this.adLoaderBuilder?.build()

        val videoOptions = VideoOptions.Builder()
            .setStartMuted(false)
            .build()

        val adOptions = NativeAdOptions.Builder()
            .setVideoOptions(videoOptions)
            .setRequestCustomMuteThisAd(false)
            .build()

        this.adLoaderBuilder?.withNativeAdOptions(adOptions)

        this.adLoaderBuilder!!.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                if (isLoadAnimationBeforeLoadAd) {
                    (avLoadingIndicatorView!!.parent as ViewGroup).removeAllViews()
                }
                inflateViews()
                setStyles(templateStyle)
                setNativeAd(nativeAd)
                adMobNativeListener?.onAdLoaded()
                AdMobLogDebug.d(TAG, "onAdLoaded()")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                adMobNativeListener?.onAdOpened()
                AdMobLogDebug.d(TAG, "onAdOpened()")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                adMobNativeListener?.onAdClosed()
                AdMobLogDebug.d(TAG, "onAdClosed()")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                adMobNativeListener?.onAdClicked()
                AdMobLogDebug.d(TAG, "onAdClicked()")
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                if (isLoadAnimationBeforeLoadAd) {
                    (avLoadingIndicatorView!!.parent as ViewGroup).removeAllViews()
                    (parent as ViewGroup).removeAllViews()
                }
                adMobNativeListener?.onAdFailedToLoad(errorCode)
                AdMobLogDebug.d(TAG, "Failed to load native ad: onAdFailedToLoad($errorCode)")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                adMobNativeListener?.onAdImpression()
                AdMobLogDebug.d(TAG, "onAdImpression()")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                adMobNativeListener?.onAdLeftApplication()
                AdMobLogDebug.d(TAG, "onAdLeftApplication()")
            }
        }).build().loadAd(adRequest)
    }

    companion object {
        private val TAG = AdMobNativeView::class.java.canonicalName

        private const val SMALL_TEMPLATE = "small_template"
        private const val MEDIUM_TEMPLATE = "medium_template"
        private const val LARGE_TEMPLATE = "large_template"

        private val loadingAnimationType = arrayOf(
            "SquareSpinIndicator",
            "LineScalePulseOutRapidIndicator",
            "BallClipRotatePulseIndicator",
            "BallSpinFadeLoaderIndicator",
            "PacmanIndicator",
            "BallClipRotatePulseIndicator",
            "LineScalePartyIndicator"
        )
    }
}