package com.google.android.ads.admob.listener;

import com.google.android.gms.ads.reward.RewardItem;

public abstract class AdMobRewardedListener {
    public void onRewardedVideoAdLoaded() {}

    public void onRewardedVideoAdOpened(){}

    public void onRewardedVideoStarted(){}

    public void onRewardedVideoAdClosed(){}

    public void onRewarded(RewardItem item){}

    public void onRewardedVideoAdLeftApplication(){}

    public void onRewardedVideoAdFailedToLoad(int fail){}

    public void onRewardedVideoCompleted(){}
}
