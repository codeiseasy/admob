package com.google.android.ads.admob.listener;

public abstract class AdMobNativeListener {

    public void onAdClosed() {
    }

    public void onAdFailedToLoad(int errorCode) {
    }

    public void onAdLeftApplication() {
    }

    public void onAdOpened() {
    }

    public void onAdLoaded() {
    }

    public void onAdClicked() {
    }

    public void onAdImpression() {
    }
}
